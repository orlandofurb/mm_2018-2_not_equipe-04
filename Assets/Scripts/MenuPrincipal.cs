﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour {

    public string sceneRight;
    public string sceneLeft;
    public bool rightSelected = true;

    public GameObject selector;
    public GameObject[] respawns;
    void Start()
    {
        if (respawns == null)
            respawns = GameObject.FindGameObjectsWithTag("selector");

        foreach (GameObject respawn in respawns)
        {
            Instantiate(selector, respawn.transform.position, respawn.transform.rotation);
        }
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown("joystick button 11") || // start
            Input.GetKeyDown(KeyCode.R))
        {
            if (rightSelected)
            {
                SceneManager.LoadScene(sceneRight);
            }
            else
            {
                SceneManager.LoadScene(sceneLeft);
            }
        }

        float lado = Input.GetAxis("Horizontal");
        bool changed = false;
        if(lado == 1)
        {
            rightSelected = true;
            changed = true;
        }
        else if(lado == -1)
        {
            rightSelected = false;
            changed = true;
        }

        if (changed)
        {
            if (rightSelected)
            {
                selector.transform.position = new Vector3(5, selector.transform.position.y, selector.transform.position.z);
                selector.transform.localEulerAngles = new Vector3(selector.transform.localEulerAngles.x, -90, selector.transform.localEulerAngles.z);
            }
            else
            {
                selector.transform.position = new Vector3(-5, selector.transform.position.y, selector.transform.position.z);
                selector.transform.localEulerAngles = new Vector3(selector.transform.localEulerAngles.x, 90, selector.transform.localEulerAngles.z);
            }
        }
    }
}

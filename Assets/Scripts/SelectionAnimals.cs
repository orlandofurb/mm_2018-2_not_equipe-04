﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionAnimals : MonoBehaviour {

    public int QuantidadeOpcoes = 5;
    public int PosicaoAtual = 0;
    public int TranslacaoSelecao = -2;
    public GameObject Seletor;
    public GameObject[] respawns;
    public bool EmMovimento = false;

    // Use this for initialization
    void Start () {
        if (respawns == null)
            respawns = GameObject.FindGameObjectsWithTag("selector");

        foreach (GameObject respawn in respawns)
        {
            Instantiate(Seletor, respawn.transform.position, respawn.transform.rotation);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        float vertical = Input.GetAxis("Vertical");
        if (!EmMovimento)
        {
            if (vertical == -1 && PosicaoAtual < 4)
            {
                PosicaoAtual++;
                Seletor.transform.position = new Vector3(
                    Seletor.transform.position.x,
                    Seletor.transform.position.y + TranslacaoSelecao,
                    Seletor.transform.position.z);
                EmMovimento = true;
            }
            else if (vertical == 1 && PosicaoAtual > 0)
            {
                PosicaoAtual--;
                Seletor.transform.position = new Vector3(
                    Seletor.transform.position.x,
                    Seletor.transform.position.y - TranslacaoSelecao,
                    Seletor.transform.position.z);
                EmMovimento = true;
            }
        }

        if (vertical != 1 && vertical != -1)
        {
            EmMovimento = false;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Geral : MonoBehaviour
{

    //Animator anim;

    public float speed = 10.0F;
    public float rotationSpeed = 100.0F;

    // Use this for initialization
    void Start ()
    {
        //anim = GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void Update ()
    {
        
        if (Input.GetKeyDown("joystick button 11") || // start
            Input.GetKeyDown(KeyCode.R))
        {
            transform.position = new Vector3(0, 5, 10);
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }
        
        //if (Input.GetKeyDown("joystick button 10") || // select
        //    Input.GetKeyDown(KeyCode.Q))
        //{
        //    float yposition = 0;
        //    //TODO Reseta camera centralizada
        //    foreach (Transform child in transform)
        //    {
        //        if (child.tag == "MainCamera")
        //        {
        //            yposition = child.transform.localEulerAngles.y;
        //            child.transform.localEulerAngles = new Vector3(
        //                        child.transform.localEulerAngles.x,
        //                        0, 
        //                        child.transform.localEulerAngles.z);
        //        }
        //        else
        //        {
        //            foreach (Transform child2 in child.transform)
        //            {
        //                if (child.tag == "MainCamera")
        //                {
        //                    yposition = child.transform.localEulerAngles.y;
        //                    child.transform.localEulerAngles = new Vector3(
        //                                child.transform.localEulerAngles.x,
        //                                0,
        //                                child.transform.localEulerAngles.z);
        //                }
        //            }
        //        }
        //    }
        //    //transform.localEulerAngles = new Vector3(
        //    //            transform.localEulerAngles.x,
        //    //            transform.localEulerAngles.y + yposition,
        //    //            transform.localEulerAngles.z);
        //}

        float frente = Input.GetAxis("Vertical");
        float lado = Input.GetAxis("Horizontal");
        //anim.SetFloat("Input X", frente);
        //anim.SetFloat("Input Z", lado);

        float translation = frente * speed;
        float rotation = lado * rotationSpeed;
        rotation *= Time.deltaTime;
        transform.Rotate(0, rotation, 0);
        
        translation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
         
    }
}
